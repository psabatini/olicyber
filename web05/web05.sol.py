import requests

# simple :  use browser and manually setting cookies in 
#           inspect element o giù di li
#           http://web-05.challs.olicyber.it/flag 

url = ' http://web-05.challs.olicyber.it/flag'


r=requests.get(url)
print(r)


cookies = dict(password='admin')
r=requests.get(url,cookies=cookies)
print(r.text)



#funziona anche così ...
headers = {'Cookie': 'password=admin'}
r=requests.get(url,headers=headers)
print(r.text)