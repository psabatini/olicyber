import requests

# simple :  use browser http://web-04.challs.olicyber.it/users 
#        :  con wireshark puoi verificare che application/xml è incluso in
#        : accept

url = 'http://web-04.challs.olicyber.it/users'

r=requests.get(url)
print(r)
# di default Accept: */*    
# per riferimenti https://stackoverflow.com/questions/14772634/what-does-accept-mean-under-client-section-of-request-headers
#  


headers = {'Accept': 'application/xml'}
r=requests.get(url,headers=headers)

print(r.text)