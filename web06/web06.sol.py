import requests

# simple :  
#           http://web-06.challs.olicyber.it/flag 
#              Missing token cookie. Unauthorized
#           http://web-06.challs.olicyber.it/token 
#              il server installa il token nel coockie (vedi inspector)
#           ora 
#           http://web-06.challs.olicyber.it/flag OKKK


s = requests.Session()
s.get('http://web-06.challs.olicyber.it/token')
r = s.get('http://web-06.challs.olicyber.it/flag')
print(r.text)
